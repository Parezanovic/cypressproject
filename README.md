# CypressProject

To run a project, you must first have a node installed on your computer.

1. Clone project on your computer
2. Navigate to project folder (Skywalk)
3. From terminal run `npm install` to install all dependencies
4. Run `npx cypress open` to open Cypress
