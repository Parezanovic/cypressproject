describe('Reaching for tequila page', () => {
    it('Successfuly load application', () => {
        cy.visit('/')
    });
    it('Hover on products and click on tequila', () => {
        cy.get('.menu-item-3830').first().trigger('mouseover')
        cy.get('.menu-item-20424').first().click()
    });
    it('Assert text and check URL', () => {
        cy.contains('Free your mind. For the essentials.').should('be.visible')
        cy.url().should('include', 'project/tequila/')
    });
});

describe('Searching for CHILI', () => {
    it('Click on seach icon', () => {
        cy.xpath('//*[@id="et_search_icon"]').click({force: true})
    });
    it('Type text "CHILI" and press enter', () => {
        cy.get('input[name="s"]').type('CHILI{enter}')
    });
    it('Assert text and check URL', () => {
        cy.contains('CHILI4 – Limited Design – “Yellow”').should('be.visible')
        cy.url().should('include', '?s=CHILI')
    });
});

describe('Searching for CHILY', () => {
    it('Click on seach icon', () => {
        cy.xpath('//*[@id="et_search_icon"]').click({force: true})
    });
    it('Clear input, type text "CHILY" and press enter', () => {
        cy.get('input[name="s"]').first().clear().type('CHILY{enter}')
    });
    it('Assert text and check URL', () => {
        cy.contains('No Results Found').should('be.visible')
        cy.url().should('include', '?s=CHILY')
    });
});